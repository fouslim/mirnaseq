#!/bin/bash
# @author: Slim Fourati (sxf279@case.edu)
# @version: 0.1

# read input arguments
email="sxf279@case.edu"
compress=false
pairEnd=false
adapterFile="/mnt/projects/SOM_PATH_RXS745U/bin/Trimmomatic-0.36/adapters"
adapterFile="${adapterFile}/TruSeq3-SE.fa"
genome=GRCh38
acceptedGenome=("GRCh38" "Mmul_1" "Mmul_8")
while getopts :d:e:g:a:m:cph option
do
    case "${option}" in
	h) echo "Command: bash miRNA.preprocess_master.sh -d {fastq/directoryfastq} ..."
	    echo "argument: d=[d]irectory with raw data (required)"
	    echo "          g=reference [g]enome"
	    echo "          a=path to FASTA files with [a]dapters to be trimmed"
	    echo "          c=raw files are [c]ompressed as DSRC files"
	    echo "          p=[p]aired-end sequencing"
	    echo "          e=[e]mail address"
	    echo "          h=print [h]elp"
	    exit 1;;
	d) dirFastq=${OPTARG};;
	e) email=${OPTARG};;
	g) genome=${OPTARG}
	    if [[ ! "${acceptedGenome[@]}" =~ "$genome" ]]
	    then
		echo "Invalid -g argument: choose between ${acceptedGenome[@]}"
		exit 1
	    fi;;
	c) compress=true;;
	p) pairEnd=true;;
	a) adapterFile=$OPTARG;;
	m) mateLength=$OPTARG;;
	\?) echo "Invalid option: -$OPTARG"
	    exit 1;;
	:)
	    echo "Option -$OPTARG requires an argument."
	    exit 1;;
    esac
done

if [ -z ${dirFastq+x} ]
then
    echo "Option -d required."
    exit 1
fi

# initialize directories
# remove trailing back slash 
dirData=$(echo $dirFastq | sed -r 's|/$||g')
dirData=$(echo $dirData | sed -r 's|/[^/]+$||g')

# find number of files and determine batches
# each batch consisting of 8 samples (16 files PE or 8 SE)
if $compress
then
    suffix="dsrc"
else
    suffix="fq.gz"
fi
nfiles=$(find $dirFastq -name "*_1.$suffix" | wc -l)
batches=$((($nfiles - 1)/8 + 1))

# make directories for every batch and move batches of 8 samples into
# their corresponding batch directory
for i in `seq 1 $batches`
do
    mkdir -p $dirData/raw$i
    if $pairEnd
    then
	find $dirFastq -name "*_1.$suffix" | sed -r 's/_1/_2/g' | \
	    head -8 | xargs -i mv "{}" "$dirData/raw$i"
    fi
    find $dirFastq -name "*_1.$suffix" | head -8 | \
        xargs -i mv "{}" "$dirData/raw$i"
done

# lauch genome indexing
sed -ri "s|^#SBATCH --mail-user=.+$|#SBATCH --mail-user=${email}|g" \
    genomeGenerate.slurm
eval $cmd

# modify preprocessing slurm script
sed -ri "s|^#SBATCH --array=1-.+$|#SBATCH --array=1-${batches}|g" \
    miRNA.preprocess_slurm.sh
# --dependency=after:$SLURM_JOB_ID

# lauch preprocessing slurm script
cmd="sbatch miRNA.preprocess_slurm.sh -d $dirData -g $genome -a $adapterFile"

if [[ ! -z $mateLength ]]
then
    cmd="$cmd -m $mateLength"
fi

if $compress
then
    cmd="$cmd -c"
fi

if $pairEnd
then
    cmd="$cmd -p"
fi

# echo $cmd
eval $cmd
