#!/bin/bash

# user email address
#SBATCH --mail-user=sxf279@case.edu

# mail is sent to you when the job starts and when it terminates or aborts
#SBATCH --mail-type=END,FAIL

# name of job
#SBATCH --job-name=preprocess_%a

# standard output file
#SBATCH --output=preprocess_%a.out

# number of nodes and processors, memory required
#SBATCH --nodes=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=32gb

# time requirements
#SBATCH --time=36:00:00

# create array
#SBATCH --array=1-BATCHES

# initialize directories
dirData=DIRDATA

# launch executable script
./miRNA.preprocess_seq.sh -d $dirData/raw$SLURM_ARRAY_TASK_ID
