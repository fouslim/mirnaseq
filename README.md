## mirnaseq
Contains scripts for miRNA-Seq preprocessing on CWRU HPC

## Dependencies
- dsrc (version 2.00 or higher) [optional]
- FastQC
- RSeQC
- Trimmomatic
- STAR aligner (version 2.5.3a or higher)
- HTSeq

## Usage
#### Add python libraries to your PATH
The different applications and libraries used to run the preprocessing pipelines
have been installed on the CWRU high-performance computing (HPC) cluster, in the
shared directory `/mnt/projects/SOM_PATH_RXS745U`. To be able to run the python
based applications (HTSeq, RSeQC) you will need to append the path to the shared
directory to the environment variable `$PYTHONPATH`.
```bash
export PYTHONPATH="$PYTHONPATH:/mnt/projects/SOM_PATH_RXS745U"
```

#### Run sRNA-Seq pipeline
The small RNA-Seq will map small RNAs (< 200bp) to a reference
genome
![picture](img/miRNA.pipeline.png)
```bash
bash miRNA.preprocessing_master.sh -d {raw_directory}

arguments:  
d=[d]irectory with raw data (directory; required)  
g=directory with the reference [g]enome  
    accepted values:GRCh38, Mmul_1, Mmul_8  
a=FASTA file with [a]dapters sequences (file)  
    default value:TruSeq3-SE.fa  
m=[m]ate length (integer)  
    if empty will be determine automatically  
c=DSRC [c]ompress files given as input  
    argument -c is not given, FASTQ files (.fq.gz) are expected  
p=[p]air-end sequencing files  
    if arugment -p is not given, single-end sequencing files are  
    expected
h=print [h]elp
```
